import json
from lib.utility import multi_key_sort, cross_join, left_join, right_join, inner_join
from operator import itemgetter


class Node(object):
    """Сomputational unit of the graph"""
    _dict_parameter = {}

    def __init__(self, node=None):
        self._table = []  # хешированные данные
        self._node = node  # предшествующий узел
        self._flag = False  # флаг, нужно ли хеширование
        self._hash_node = False  # флаг, что пройден узел

    def _create_hash(self, data):
        """хешируем данные, если есть флаг"""
        if self._flag:
            for row in data:
                self._table.append(row)
                yield row
        else:
            yield from data

    def _run(self):
        """
        если узел проходим первый раз, то читаем данные,
            иначе считываем хеш
        """
        if not self._hash_node:
            yield from self._read_row()
            self._hash_node = True
        else:
            for row in self._table:
                yield row


class Variable(Node):
    """Variable for reading data from a file"""

    def __init__(self, source):
        super().__init__()
        self._source = source

    def _read_row(self):
        main_input = self._dict_parameter[self._source]

        for line in main_input:
            row = json.loads(line)
            if self._flag:
                self._table.append(row)
            yield row
        main_input.close()


class Fold(Node):
    """The computational node of the graph that considers the FOLD for the given table"""

    def __init__(self, node, source, state):
        super().__init__(node)
        self._fold = source
        self._state = state

    def _read_row(self):
        result = self._state
        for row in self._node._run():
            result = self._fold(result, row)
        if self._flag:
            self._table.append(result)
        yield result


class Map(Node):
    """The computational node of the graph that considers the MAP for the given table"""

    def __init__(self, node, source):
        super().__init__(node)
        self._map = source

    def _read_row(self):
        for row in self._node._run():
            yield from self._create_hash(self._map(row))


class Reduce(Node):
    """The computational node of the graph that considers the REDUCE for the given table"""

    def __init__(self, node, source, keys, flag_sort=True):
        super().__init__(node)
        self._node = node
        self._reduce = source
        self._keys = keys
        self._flag_sort = flag_sort

    def _read_row(self):
        if self._flag_sort:
            list_row = multi_key_sort(self._node._run(), self._keys)
        else:
            list_row = self._node._run()

        compares = list(map(itemgetter, self._keys))
        cmp = lambda a, b: (a > b) - (a < b)

        def compare(left, right):
            """сравнивает строки таблицы по заданным ключам"""
            compare_iter = (
                cmp(fn(left), fn(right))
                for fn in compares
            )
            return next((el for el in compare_iter if el), 0)

        elem = None
        list_eq_keys = []

        for row in list_row:

            if elem is None:
                list_eq_keys.append(row)
                elem = row
                continue

            if compare(elem, row):
                yield from self._create_hash(
                    self._reduce(list_eq_keys)
                )
                list_eq_keys = []

            list_eq_keys.append(row)
            elem = row

        else:
            if list_eq_keys:
                yield from self._create_hash(
                    self._reduce(list_eq_keys)
                )


class Sort(Node):
    """The computational node of the graph that sorts the table according to the given keys"""

    def __init__(self, node, source):
        super().__init__(node)
        self._node = node
        self._source = source

    def _read_row(self):
        data = multi_key_sort(self._node._run(), self._source)
        if self._flag:
            self._table = data

        for row in data:
            yield row


class Join(Node):
    """The computational node of the graph that connects two tables according to a given key"""

    _dict_join = {
        'cross': cross_join,
        'left': left_join,
        'right': right_join,
        'inner': inner_join
    }

    def __init__(self, node_left, node_right, strategy, keys=None):
        super().__init__(node_left)
        self._node_right = node_right
        self._join = self._dict_join[strategy]
        self._keys = keys

    def _read_row(self):
        if self._keys:
            data = self._join(
                multi_key_sort(self._node._run(), self._keys),
                multi_key_sort(self._node_right._run(), self._keys),
                self._keys
            )
        else:
            data = []
            self._join(
                list(self._node._run()),
                list(self._node_right._run()),
                data
            )
        if self._flag:
            self._table = data

        for row in data:
            yield row


class ComputeGraph(object):
    """A computational graph that starts computational nodes in the order they are added to the graph"""

    def __init__(self, input_node, output_node):
        self._input = input_node  # входные узлы вида Variable
        self._output = output_node  # выходной узел

    def run(self, **kwarg):
        """
        обходим каждый узел графа и помечаем те, которые используются несколько раз,
        затем запускаем вычисление графа и записываем результат выходного узла
        """

        for data in self._input:
            data._dict_parameter = kwarg

        file_out = kwarg['save_result']
        set_node = dict()

        stack_node = [self._output]

        while stack_node:
            tmp_node = stack_node.pop()
            count = set_node.get(tmp_node, 0)

            if count == 0:
                set_node[tmp_node] = 1

                if tmp_node._node:
                    stack_node.append(tmp_node._node)

                    if isinstance(tmp_node, Join):
                        stack_node.append(tmp_node._node_right)
            elif count == 1:
                set_node[tmp_node] = 2

        for node, count in set_node.items():
            if count == 2:
                node._flag = True

        for row in self._output._run():
            file_out.write(
                json.dumps(row) + '\n'
            )
        file_out.close()
