from itertools import groupby
from operator import itemgetter
from functools import cmp_to_key


def multi_key_sort(items, columns):
    """Sorting an array with multiple keys"""
    compares = list(map(itemgetter, columns))
    cmp = lambda a, b: (a > b) - (a < b)

    def compare(left, right):
        compare_iter = (
            cmp(fn(left), fn(right))
            for fn in compares
        )
        return next((result for result in compare_iter if result), 0)

    return sorted(items, key=cmp_to_key(compare))


def cross_join(left, right, result):
    """ CROSS JOIN operation of two tables """

    for row_l in left:
        for row_r in right:
            result.append(
                {**row_l, **row_r}
            )


def inner_join(left_table, right_table, key):
    """ LEFT JOIN operation of two tables """

    result = []

    compares = list(map(itemgetter, key))
    keyfunc = lambda x: tuple(fn(x) for fn in compares)

    iter_left_groups = groupby(left_table, keyfunc)
    iter_right_groups = groupby(right_table, keyfunc)

    next_left_groups = next(iter_left_groups, None)
    next_right_groups = next(iter_right_groups, None)

    while next_left_groups and next_right_groups:

        key_l, gr_l = next_left_groups
        key_r, gr_r = next_right_groups
        if key_l == key_r:
            cross_join(list(gr_l), list(gr_r), result)

            next_left_groups = next(iter_left_groups, None)
            next_right_groups = next(iter_right_groups, None)
        elif key_l < key_r:
            next_left_groups = next(iter_left_groups, None)
        else:
            next_right_groups = next(iter_right_groups, None)

    return result


def left_join(left_table, right_table, key):
    """ LEFT JOIN operation of two tables """

    result = []

    compares = list(map(itemgetter, key))
    keyfunc = lambda x: tuple(fn(x) for fn in compares)

    iter_left_groups = groupby(left_table, keyfunc)
    iter_right_groups = groupby(right_table, keyfunc)

    next_left_groups = next(iter_left_groups, None)
    next_right_groups = next(iter_right_groups, None)

    default = dict.fromkeys(right_table[0].keys())

    while next_left_groups and next_right_groups:

        key_l, gr_l = next_left_groups
        key_r, gr_r = next_right_groups

        if key_l == key_r:
            cross_join(list(gr_l), list(gr_r), result)
            next_left_groups = next(iter_left_groups, None)
            next_right_groups = next(iter_right_groups, None)
        elif key_l < key_r:
            cross_join([default], list(gr_l), result)
            next_left_groups = next(iter_left_groups, None)
        else:
            next_right_groups = next(iter_right_groups, None)

    while next_left_groups:
        cross_join([default], list(next_left_groups[1]), result)
        next_left_groups = next(iter_left_groups, None)

    return result


def right_join(left, right, key):
    """ RIGHT JOIN operation of two tables """
    return left_join(right, left, key)
