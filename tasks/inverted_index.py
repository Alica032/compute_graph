from collections import Counter
import math
from lib import mrop
import re


def tokenizer_mapper(document_text):
    text_string = document_text['text'].lower()[:100]
    match_pattern = re.findall(r'\b[a-z]{3,15}\b', text_string)

    for token in match_pattern:
        yield {
            'doc_id': document_text['doc_id'],
            'word': token,
        }


def count_rows(state, record):
    for column in state:
        state[column] += 1
    return state


def term_frequency_reducer(record):
    yield {
        'doc_id': record[0]['doc_id'],
        'word': record[0]['word']
    }


def calc_idf(record):
    yield {
        'word': record[0]['word'],
        'freq': record[0]['docs_count'] / len(record)
    }


def tf(records):
    word_count = Counter()
    for r in records:
        word_count[r['word']] += 1

    total = sum(word_count.values())
    for w, count in word_count.items():
        yield {
            'doc_id': records[0]['doc_id'],
            'word': w,
            'tf': count / total
        }


def invert_index(record):
    tfidf = []

    for row in record:
        tfidf.append(row['tf'] * math.log(row['freq']))

    args = arg3max(tfidf.copy())
    yield {
        'word': record[0]['word'],
        'index': [tuple([record[i]['doc_id'], tfidf[i]]) for i in args]
    }


def arg3max(data):
    args = []
    max_el = max(data)
    while len(args) < 3 and max_el > 0:
        for i, elem in enumerate(data):
            if elem == max_el:
                args.append(i)
                data[i] = -1
        max_el = max(data)

    return args[:3]


if __name__ == '__main__':
    x = mrop.Variable(source='main_input')

    split_word = mrop.Map(x, tokenizer_mapper)
    count_docs = mrop.Fold(x, count_rows, {'docs_count': 0})

    count_idf = mrop.Join(count_docs,
                          mrop.Reduce(split_word,
                                      term_frequency_reducer,
                                      ('doc_id', 'word')),
                          strategy='cross'
                          )

    count_idf = mrop.Reduce(count_idf, calc_idf, keys=['word'])

    calc_index = mrop.Join(mrop.Reduce(split_word, tf, keys=['doc_id']),
                           count_idf,
                           strategy='left',
                           keys=['word']
                           )

    calc_index = mrop.Reduce(calc_index, invert_index, keys=['word'])

    compGraph = mrop.ComputeGraph([x], calc_index)
    compGraph.run(
        verbose=True,
        main_input=open('data/text_corpus.txt'),
        save_result=open('result/tf-idf.txt', 'w')
    )
