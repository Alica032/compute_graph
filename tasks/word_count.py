import re

from lib import mrop


def tokenizer_mapper(document_text):
    frequency = {}
    text_string = document_text['text'].lower()
    match_pattern = re.findall(r'\b[a-z]{1,15}\b', text_string)

    for word in match_pattern:
        count = frequency.get(word, 0)
        frequency[word] = count + 1

    yield {
        'doc_id': document_text['doc_id'],
        'freq_word': frequency
    }


def sum_frequency(state, record):
    for column in state:
        freq_word = state[column]
        record_freq_word = record[column]

        for word in record_freq_word.keys():
            freq_word[word] = freq_word.get(word, 0) + record_freq_word[word]

    return state

if __name__ == '__main__':

    x = mrop.Variable(source='main_input')
    y = mrop.Fold(mrop.Map(x, tokenizer_mapper), sum_frequency, {'freq_word': {}})

    compGraph = mrop.ComputeGraph([x], y)
    compGraph.run(
        main_input=open('data/text_corpus.txt'),
        save_result=open('result/word_count.txt', 'w')
    )
