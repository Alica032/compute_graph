from collections import Counter
import math
from lib import mrop
import re


def tokenizer_mapper(document_text):
    document_text['text'] = document_text['text']
    text_string = document_text['text'].lower()
    match_pattern = re.findall(r'\b[a-z]{4,15}\b', text_string)

    for token in match_pattern:
        yield {
            'doc_id': document_text['doc_id'],
            'word': token,
        }


def set_doc(state, record):
    el = record['doc_id']
    state['set_doc'][el] = state['set_doc'].get(el, 0) + 1
    return state


def pmi_word(records):
    word_count = Counter()
    for r in records:
        word_count[r['doc_id']] += 1
    flag = False

    if len(word_count) > 0:
        flag = True
        for r in records:
            if word_count[r['doc_id']] < 2:
                flag = False
                break

    if flag:
        all_w = records[0]['set_doc']
        total_w = sum(all_w.values())

        for id_doc, n_word in word_count.items():
            word_count_all_doc = sum(word_count.values())
            yield {
                'doc_id': id_doc,
                'word': records[0]['word'],
                'pmi': math.log(n_word * total_w / all_w[id_doc] / word_count_all_doc)
            }


def arg10max(data):
    args = []
    max_el = max(data)
    while len(args) < 10 and max_el > -10000:
        for i, elem in enumerate(data):
            if elem == max_el:
                args.append(i)
                data[i] = -10000
        max_el = max(data)

    return args[:10]


def top_pmi(records):
    pmi = []
    for row in records:
        pmi.append(row['pmi'])

    args = arg10max(pmi.copy())
    yield {
        'doc_id': records[0]['doc_id'],
        'top_pmi': [tuple([records[i]['word'], pmi[i]]) for i in args]
    }


if __name__ == '__main__':

    mrop.Node.dict_parameter['main_input'] = open('data/text_corpus.txt')
    x = mrop.Variable(source='main_input')
    split_word = mrop.Map(x, tokenizer_mapper)

    doc_n = mrop.Fold(split_word, set_doc,
                      {'set_doc': dict()})

    h = mrop.Join(split_word, doc_n, strategy='cross')

    pmi = mrop.Reduce(
        mrop.Join(split_word, doc_n, strategy='cross'),
        pmi_word,
        keys=['word']
    )

    output = mrop.Reduce(pmi, top_pmi, keys=['doc_id'])

    compGraph = mrop.ComputeGraph([x], output)

    compGraph.run(
        verbose=True,
        main_input=open('data/text_corpus.txt'),
        save_result=open('result/pmi.txt', 'w')
    )
