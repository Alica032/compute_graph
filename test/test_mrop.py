import unittest
import sys
import json

sys.path.append('..')
from lib import mrop
from lib import utility


class TestCOMP(unittest.TestCase):
    def test_variable(self):
        input_data = []
        with open('test_data.txt', 'w') as file:
            for i in range(20):
                row = {'id_doc': (i % 6), 'text': (40 - i) % 4}
                input_data.append(row)
                file.write(
                    json.dumps(row) + '\n'
                )

        x = mrop.Variable(source='main_input')

        compGraph = mrop.ComputeGraph([x], x)
        compGraph.run(
            verbose=True,
            main_input=open('test_data.txt'),
            save_result=open('result_data.txt', 'w')
        )

        output_data = []
        with open('result_data.txt') as file:
            for line in file:
                output_data.append(json.loads(line))

        self.assertEquals(input_data, output_data)

    def test_join_left(self):
        count_el = [(20, 25), (25, 20), (20, 20)]

        for n_l, n_r in count_el:
            table_l = []
            for i in range(n_l):
                for j in range(2):
                    table_l.append({'id_doc': (i % 3), 'text1': i + j, 'id': i})

            table_r = []
            for i in range(n_r):
                for j in range(3):
                    table_r.append({'id_doc': (i % 5), 'text2': i + j * 2, 'id': i})

            res_join = utility.left_join(table_l, table_r, ['id', 'id_doc'])

            for l in table_l:
                for r in table_r:
                    if l['id_doc'] == r['id_doc'] and l['id'] == r['id']:
                        d = {**l, **r}
                        assert (d in res_join)

            for row in res_join:
                if row['text2'] is None:
                    for r in table_r:
                        assert (row['id_doc'] != r['id_doc'] or row['id'] != r['id'])

    def test_join_right(self):
        count_el = [(20, 25), (25, 20), (20, 20)]

        for n_l, n_r in count_el:
            table_l = []
            for i in range(n_l):
                for j in range(2):
                    table_l.append({'id_doc': (i % 3), 'text1': i + j, 'id': i})

            table_r = []
            for i in range(n_r):
                for j in range(3):
                    table_r.append({'id_doc': (i % 5), 'text2': i + j * 2, 'id': i})

            res_join = utility.left_join(table_l, table_r, ['id', 'id_doc'])

            for l in table_l:
                for r in table_r:
                    if l['id_doc'] == r['id_doc'] and l['id'] == r['id']:
                        d = {**l, **r}
                        assert (d in res_join)

            for row in res_join:
                if row['text1'] is None:
                    for l in table_l:
                        assert (row['id_doc'] != l['id_doc'] or row['id'] != l['id'])

    def test_inner_join(self):
        count_el = [(20, 25), (25, 20), (20, 20)]

        for n_l, n_r in count_el:
            table_l = []
            for i in range(n_l):
                for j in range(2):
                    table_l.append({'id_doc': (i % 3), 'text1': i + j, 'id': i})

            table_r = []
            for i in range(n_r):
                for j in range(3):
                    table_r.append({'id_doc': (i % 5), 'text2': i + j * 2, 'id': i})

            res_join = utility.inner_join(table_l, table_r, ['id', 'id_doc'])

            for l in table_l:
                for r in table_r:
                    if l['id_doc'] == r['id_doc'] and l['id'] == r['id']:
                        d = {**l, **r}
                        assert (d in res_join)

            for row in res_join:
                assert ({'id_doc': row['id_doc'], 'id': row['id'], 'text1': row['text1']} in table_l)
                assert ({'id_doc': row['id_doc'], 'id': row['id'], 'text2': row['text2']} in table_r)

    def test_reduce(self):

        def term_frequency_reducer(record):
            yield {
                'id_doc': record[0]['id_doc'],
                'text': record[0]['text']
            }

        with open('test_data.txt', 'w') as file:
            for i in range(20):
                file.write(
                    json.dumps(
                        {'id_doc': (i % 6), 'text': (40 - i) % 4}) + '\n'
                )

        x = mrop.Variable(source='main_input')
        y = mrop.Reduce(x, term_frequency_reducer, ('id_doc', 'text'))

        compGraph = mrop.ComputeGraph([x], y)
        compGraph.run(
            verbose=True,
            main_input=open('test_data.txt'),
            save_result=open('result_data.txt', 'w')
        )

        table = []
        with open('result_data.txt') as file:
            for line in file:
                table.append(json.loads(line))

        for i, row in enumerate(table):
            for other in table[i + 1:]:
                if row['id_doc'] == other['id_doc']:
                    self.assertLess(row['text'], other['text'])
                else:
                    self.assertLess(row['id_doc'], other['id_doc'])


if __name__ == '__main__':
    unittest.main()
